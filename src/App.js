import { useState } from "react";
import "./App.css";
import Image from '../src/study.jpg'
import "./Book.css";

import { Picker } from "emoji-mart";


function App() {
  const [input, setInput] = useState("");
  const [showEmojis, setShowEmojis] = useState(false);

  const addEmoji = (e) => {
    let sym = e.unified.split("-");
    let codesArray = [];
    sym.forEach((el) => codesArray.push("0x" + el));
    let emoji = String.fromCodePoint(...codesArray);
    setInput(input + emoji);
  };

  return (
    <div className="container">
    <div className="app">

      {showEmojis && (

        <Picker onSelect={addEmoji} />

      )}
      <div id="wrapper" >
        <div id="container">
          <section className="open-book" style={{ backgroundImage: `url(${Image})` }}>

            <article>
              <div className="imoji-wrapper">
                <div className="imoji-inner">
                  <button className="button" onClick={() => setShowEmojis(!showEmojis)}>
                    😊
                  </button>
                  <input
                    value={input}
                    onChange={(e) => setInput(e.target.value)}
                    type="text"
                    placeholder="Start Typing Here"
                  />

                </div>


              </div>

            </article>

          </section>
        </div>
      </div>

</div>
    </div>
  );
}

export default App;